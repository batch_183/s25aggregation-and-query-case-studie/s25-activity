//ITEM No. 2 
db.fruitsOnSale.aggregate([
    {$match: {"onSale": true}},
    {$count: "name"}
])

//ITEM No. 3 (NOT FINISH)

db.fruitsOnSale.aggregate([
      {$match: {"enough stocks": {$gte: 20}}},
      {$count: "stocks"}])


// ITEM No. 4 (Average / Supplier)
db.fruitsOnSale.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "Average_Price": {$avg: "$price"}}}
]);


// ITEM No. 5 (Highest Price / Supplier)
db.fruitsOnSale.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "Max_Price": {$max: "$price"}}}
]);

//ITEM No. 6 (Min Price / Supplier)

db.fruitsOnSale.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "Min_Price": {$min: "$price"}}}
]);

